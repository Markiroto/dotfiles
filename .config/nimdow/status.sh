#!/bin/bash

# DISK

hddicon() {
	echo "💽"
	}

hdd() {
	  free="$(df -h /home | grep /dev | awk '{print $3}' | sed 's/G/Gb/')"
      perc="$(df -h /home | grep /dev/ | awk '{print $5}')"
      echo "$perc($free) "
    }

# RAM
memicon() {
    echo "💾"
}

mem() {
used="$(free | grep Mem: | awk '{print $3}')"
total="$(free | grep Mem: | awk '{print $2}')"
human="$(free -h | grep Mem: | awk '{print $3}' | sed s/i//g)"

ram="$(( 200 * $used/$total - 100 * $used/$total ))%($human) "

echo "$ram"
}

#VOLUME
volume() {
    vol="$(pamixer --get-volume-human)"
    if [[ "$vol" == "muted" ]]; then
        printf "🔇 %s" "$vol"
    else
        printf "🎧 %s" "$vol"
    fi
}

#BATTERY
bat() {
batstat="$(cat /sys/class/power_supply/BAT0/status)"
battery="$(cat /sys/class/power_supply/BAT0/capacity)"
    if [ $batstat = 'Unknown' ]; then
    batstat="🔌"
    elif [ $batstat = 'Charging' ]; then
    batstat="🔌"
    else
    batstat="🔋"
fi

echo "$batstat $battery %"
}

#PACKAGES
kernelicon() {
	echo "🐧"
}

kernelinfo() {
	kernel="$(uname -r)"
	echo "$kernel"
}

#DATE
dateicon() {
	echo "⏰"
}

dateinfo() {
    echo "$(date "+%T %b %d %Y (%a)")"
}

#CPU
cpuicon() {
    echo "🧠"
}

cpu() {
	  read cpu a b c previdle rest < /proc/stat
	    prevtotal=$((a+b+c+previdle))
	      sleep 0.5
	        read cpu a b c idle rest < /proc/stat
		  total=$((a+b+c+idle))
		    cpu=$((100*( (total-prevtotal) - (idle-previdle) ) / (total-prevtotal) ))
            echo "$cpu%"
	      }

while true; do
	xsetroot -name "$(kernelicon) $(kernelinfo) | $(hddicon) $(hdd)| $(memicon) $(mem) | $(cpuicon) $(cpu) | $(volume) | $(bat) | $(dateicon) $(dateinfo)"
	sleep 1
done
