#!/usr/bin/env bash

swaylock \
	--screenshots \
	--effect-blur 7x5 \
	--effect-vignette 0.5:0.5 \
	--fade-in 0.2
  --ring-color d79921 \
	--key-hl-color d79921 \
	--line-color 000000 \
	--inside-color 282828 \
	--separator-color d79921 
