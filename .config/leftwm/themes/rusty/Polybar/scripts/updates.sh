#!/bin/bash

upgrades() {
	upgrades="$(aptitude search '~U' | wc -l)"
	echo "$upgrades updates"
}

echo $(upgrades)
