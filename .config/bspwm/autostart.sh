~/.config/polybar/launch.sh &
sxhkd &
dunst &
nitrogen --restore &
lxpolkit &
picom --experimental-backends &
xsetroot -cursor_name left_ptr &
