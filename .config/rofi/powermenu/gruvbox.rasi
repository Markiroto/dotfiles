/**
 *
 * Author : Aditya Shakya (adi1090x)
 * Github : @adi1090x
 * 
 * Rofi Theme File
 * Rofi Version: 1.7.3
 **/

/*****----- Configuration -----*****/
configuration {
    show-icons:                 false;
}

/*****----- Global Properties -----*****/
* {
    gruvbox-dark-bg0:          #282828;
    gruvbox-dark-bg0-soft:     #32302f;
    gruvbox-dark-bg3:          #665c54;
    gruvbox-dark-fg0:          #fbf1c7;
    gruvbox-dark-fg1:          #ebdbb2;
    gruvbox-dark-red-dark:     #cc241d;
    gruvbox-dark-red-light:    #fb4934;
    gruvbox-dark-yellow-dark:  #d79921;
    gruvbox-dark-yellow-light: #fabd2f;
    gruvbox-dark-gray:         #a89984;

    /* Theme colors */
    background:                  @gruvbox-dark-bg0;
    background-alt:              @gruvbox-dark-bg0;
    background-color:            @background;
    foreground:                  @gruvbox-dark-fg1;
    border-color:                @gruvbox-dark-yellow-dark;
    separatorcolor:              @border-color;
    scrollbar-handle:            @border-color;

    normal-background:           @background;
    normal-foreground:           @foreground;
    alternate-normal-background: @background;
    alternate-normal-foreground: @foreground;
    selected-normal-background:  @gruvbox-dark-yellow-dark;
    selected-normal-foreground:  @gruvbox-dark-bg0;

    active:                      @gruvbox-dark-yellow-dark;
    active-foreground:           @background;
    alternate-active-background: @active-background;
    alternate-active-foreground: @active-foreground;
    selected:  @gruvbox-dark-yellow-light;
    selected-active-foreground:  @active-foreground;

    urgent:                      @gruvbox-dark-red-dark;
    urgent-foreground:           @background;
    alternate-urgent-background: @urgent-background;
    alternate-urgent-foreground: @urgent-foreground;
    selected-urgent-background:  @gruvbox-dark-red-light;
    selected-urgent-foreground:  @urgent-foreground;
}

/*
USE_BUTTONS=NO
*/

/*****----- Main Window -----*****/
window {
    transparency:                "real";
    location:                    center;
    anchor:                      center;
    fullscreen:                  false;
    width:                       1000px;
    x-offset:                    0px;
    y-offset:                    0px;

    padding:                     0px;
    border:                      4px solid;
    border-radius:               24px;
    border-color:                @border-color;
    cursor:                      "default";
    background-color:            @background;
}

/*****----- Main Box -----*****/
mainbox {
    background-color:            transparent;
    orientation:                 horizontal;
    children:                    [ "imagebox", "listview" ];
}

/*****----- Imagebox -----*****/
imagebox {
    spacing:                     20px;
    padding:                     20px;
    background-color:            transparent;
    background-image:            url("~/.config/rofi/powermenu/image.jpg", height);
    children:                    [ "inputbar", "dummy", "message" ];
}

/*****----- User -----*****/
userimage {
    margin:                      0px 0px;
    border:                      10px;
    border-radius:               10px;
    border-color:                @background-alt;
    background-color:            transparent;
    background-image:            url("~/.config/rofi/powermenu/image.jpg", height);
}

/*****----- Inputbar -----*****/
inputbar {
    padding:                     15px;
    border-radius:               100%;
    background-color:            #00282828;
    text-color:                  @foreground;
    children:                    [ "dummy", "prompt", "dummy"];
}

dummy {
    background-color:            transparent;
}

prompt {
    background-color:            inherit;
    text-color:                  inherit;
}

/*****----- Message -----*****/
message {
    enabled:                     true;
    margin:                      0px;
    padding:                     15px;
    border-radius:               100%;
    background-color:            #10282828;
    text-color:                  @foreground;
}
textbox {
    background-color:            inherit;
    text-color:                  inherit;
    vertical-align:              0.5;
    horizontal-align:            0.5;
}

/*****----- Listview -----*****/
listview {
    enabled:                     true;
    columns:                     3;
    lines:                       2;
    cycle:                       true;
    dynamic:                     true;
    scrollbar:                   false;
    layout:                      vertical;
    reverse:                     false;
    fixed-height:                true;
    fixed-columns:               true;
    
    spacing:                     20px;
    margin:                      20px;
    background-color:            transparent;
    cursor:                      "default";
}

/*****----- Elements -----*****/
element {
    enabled:                     true;
    padding:                     60px 10px;
    border-radius:               100%;
    background-color:            @background-alt;
    text-color:                  @foreground;
    cursor:                      pointer;
}
element-text {
    font:                        "feather bold 12";
    background-color:            transparent;
    text-color:                  inherit;
    cursor:                      inherit;
    vertical-align:              0.5;
    horizontal-align:            0.5;
}
element selected.normal {
    background-color:            var(selected);
    text-color:                  var(background);
}
