#!/bin/bash

chosen=$(echo -e "[Cancel]\nLogout\nShutdown\nReboot\nSuspend\nLock" | rofi -dmenu -i -p "Exit?" -lines 6)

if [[ $chosen = "Logout" ]]; then
        loginctl terminate-session self
elif [[ $chosen = "Shutdown" ]]; then
	systemctl poweroff
elif [[ $chosen = "Reboot" ]]; then
	systemctl reboot
elif [[ $chosen = "Suspend" ]]; then
	sleep 1 && systemctl suspend && slock
elif [[ $chosen = "Lock" ]]; then
	sleep 1 && slock
fi
