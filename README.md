# Dotfiles

My Trash Configurations for Applications :D

![rice.png](https://gitlab.com/Markiroto/dotfiles/-/raw/main/rice.png)
BSPWM Screenshot

## What are Dotfiles?

Dotfiles are configurations files in a computer on Linux or Unix-Based systems. They are called dotfiles cause they have dots in front of them :D

## Things Used
1. [Polybar](https://github.com/polybar/polybar)
2. [Fontisto](https://www.fontisto.com/)
3. [Rofi](https://github.com/davatorium/rofi)
4. [Nerd Fonts](https://www.nerdfonts.com)
